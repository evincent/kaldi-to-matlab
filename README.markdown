This page provides a set of software tools to read and write the following Kaldi file formats in Matlab:
* features
* GMM/HMM acoustic models (read only)
* DNN acoustic models
* lattices
* N-best lists
* alignments (read only)

# Licence and citation
This software was authored by Emmanuel Vincent (Inria) and Shinji Watanabe (MERL) and is distributed under the terms of the [GNU Public License version 3](http://www.gnu.org/licenses/gpl.txt).

If you use this software in a publication, please cite
> Emmanuel Vincent and Shinji Watanabe, Kaldi to Matlab conversion tools, http://kaldi-to-matlab.gforge.inria.fr/, 2014.

# Download
* [kaldi-to-matlab.tar.gz](kaldi-to-matlab.tar.gz)

# Install
* Get Matlab

* Preload the most recent version of libstdc++ and libgfortran installed on your system. E.g., add the following line to your .bash_profile (change the path as appropriate):
> export LD_PRELOAD=/usr/lib64/libstdc++.so.6:/usr/lib64/libgfortran.so.3

Note: the environment variable LD_PRELOAD must be defined before starting Matlab, as the C++ and Fortran libraries included in Matlab are outdated and do not allow execution of Kaldi commands within Matlab.

* Install Kaldi

Note: this software was tested with version 4073 of kaldi-trunk. It is likely to work with older or younger versions too, but this is not guaranteed due to changes in the Kaldi file formats over time.

* Add to your path the Kaldi executables convert-ali, copy-feats, gmm-copy, lattice-copy, and nnet-copy, and the Kaldi scripts int2sym.pl and sym2int.pl. E.g., add the following lines to your .bash_profile (change the path as appropriate):
> export KALDI_ROOT=/home/mylogin/kaldi-trunk\n
> export PATH=$KALDI_ROOT/src/bin:$KALDI_ROOT/src/featbin:$KALDI_ROOT/src/gmmbin:$KALDI_ROOT/src/latbin:$KALDI_ROOT/src/nnetbin:$KALDI_ROOT/egs/wsj/s5/utils

# Use

To get information about each function, after the Matlab prompt, type 'help' followed by the name of the function, e.g.,
> help readkaldifeatures